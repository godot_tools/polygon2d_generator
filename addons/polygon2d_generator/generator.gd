# do weights generation
#	stop add sync bones
#	add weight parameters + place some to gui

# do skeleton generation 
#	1. chain
#		- bone length
#	2. mesh
#		- dencity

# do separate internal vertex creation 
# do separate polygon creation 
# do generation parenting and position independence

tool
extends Control

var editor : EditorInterface
var step := 50
var offset := 20
var plugin
var polygon2d : Polygon2D
var is_active := false
var area : Dictionary
var props : Dictionary
var init_polygon2d_data : Dictionary
var memory = {}


func _enter_tree() -> void:
	$step/label.set_text(str(step))
	$skeleton_type.add_item('chain')
	$skeleton_type.add_item('mesh')
	
	
func _offset(sp : Sprite) -> void:
	if sp.centered:
		sp.centered = false
		sp.position.x -= sp.texture.get_width() / 2
		sp.position.y -= sp.texture.get_height() / 2
	

func _on_offset_pressed() -> void:
	var nodes = _selected_nodes()
	if nodes:
		if nodes.size() > 1:
			for node in nodes:
				if node is Sprite:
					_offset(node)
		else:
			if nodes[0] is Sprite:
				_offset(nodes[0])
			else:
				for ch in nodes[0].get_children():
					if ch is Sprite:
						_offset(ch)


func _convert(sp) -> Polygon2D:
	var bm = BitMap.new()
	var poly = Polygon2D.new()
	bm.create_from_image_alpha(sp.texture.get_data())
	var rect = Rect2(-50, -50, sp.texture.get_width() + 100, sp.texture.get_height() + 100)
	var pre_arr = bm.opaque_to_polygons(rect,10)
	var arr = Geometry.offset_polygon_2d(PoolVector2Array(pre_arr[0]), offset)
	
	poly.set_texture(sp.texture)
	poly.set_polygon(PoolVector2Array(arr[0]))
	poly.name = sp.name + '_poly'
	poly.position = sp.position
	if not $duplicate.pressed:
		sp.queue_free()
	sp.get_parent().add_child(poly)
	poly.set_owner(get_tree().edited_scene_root)
	
	return poly


func _on_convert_pressed() -> void:
	var nodes = _selected_nodes()
	if nodes:
		if nodes.size() > 1:
			for node in nodes:
				if node is Sprite:
					_convert(node)
		else:
			if nodes[0] is Sprite:
				_convert(nodes[0])
			else:
				for ch in nodes[0].get_children():
					if ch is Sprite:
						_convert(ch)


# _________________________________________	

func _on_step_value_changed(value) -> void:
	step = value
	$step/label.set_text(str(step))
	

func _on_offset_value_changed(value) -> void:
	offset = value
	$offset/label.text = str(offset)


func _on_clear_pressed() -> void:
	for node in _selected_nodes():
		_clear_polygon(node)
		
		
func _clear_polygon(node) -> void:
	if node is Polygon2D:
		_update_polygon2d(node)
		if _is_polygon_in_memory():
			polygon2d.set_polygon(_get_polygon_in_memory())
		polygon2d.set_polygons([])
		polygon2d.set_internal_vertex_count(0)
		polygon2d.set_uv(PoolVector2Array([]))
		_end()

func _get_polygon_in_memory() -> PoolVector2Array:
	if _is_polygon_in_memory():
		return memory[polygon2d.get_instance_id()]
	else:
		return PoolVector2Array([])


func _is_polygon_in_memory() -> bool:
	if memory.has(polygon2d.get_instance_id()):
		return true
	else:
		return false
		
		
func _remember_polygon2d() -> void:
	memory[polygon2d.get_instance_id()] = PoolVector2Array(polygon2d.get_polygon())


func _on_generate_polygon() -> void:
	for node in _selected_nodes():
		_generate_polygon(node)
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
		
		
func _generate_polygon(node):
	var poly : Polygon2D
	if node is Polygon2D:
		poly = node
	elif node is Sprite:
		_offset(node)
		poly = _convert(node)
	
	if poly:
		_clear_polygon(poly)
		_update_polygon2d(poly)
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
		_create_per_vert()
		_create_internal_vert()
		_triangulate_polygon()
		_end()


func _create_basic_poly() -> void:
	if polygon2d.get_polygon().size() < 2:
		var bm = BitMap.new()
		bm.create_from_image_alpha(polygon2d.texture.get_data())
		var rect = Rect2(0, 0, polygon2d.texture.get_width(), polygon2d.texture.get_height())
		var poly = bm.opaque_to_polygons(rect, 2)
		polygon2d.set_polygon(PoolVector2Array(poly[0]))
		polygon2d.set_polygons(PoolVector2Array(poly[0]))



func _sort_nearest_point(a, b) -> bool:
	if a[1] < b[1]:
		return true
	else:
		return false


func _update_polygon2d(node : Polygon2D) -> void:
	polygon2d = node
	if not _is_polygon_in_memory():
		_remember_polygon2d()
	elif polygon2d.internal_vertex_count == 0 and polygon2d.get_polygons().size() == 0:
		_remember_polygon2d()
	_define_area()
	_set_props()
	_update_init_poly()
	
	
func _update_init_poly() -> void:
	init_polygon2d_data = {
		'polygon' : polygon2d.get_polygon(),
		'uv' : polygon2d.get_uv(),
		'vertex_colors' : polygon2d.get_vertex_colors(),
		'polygons' : polygon2d.get_polygons(),
		'internal_vertex_count' : polygon2d.get_internal_vertex_count()
	}
	
	
func _return_init_poly() -> void:
	polygon2d.polygon = init_polygon2d_data.polygon
	polygon2d.uv = init_polygon2d_data.uv
	polygon2d.vertex_colors = init_polygon2d_data.vertex_colors
	polygon2d.polygons = init_polygon2d_data.polygons
	polygon2d.internal_vertex_count = init_polygon2d_data.internal_vertex_count
	
	
func _end() -> void:
	if area.area:
		area.area.queue_free()
		area.area = null
	if area.coll:
		area.coll = null
	
	
func _set_props() -> void:
	var lim = area.limits
	props = {}
	props.size = Vector2(lim.max.x - lim.min.x, lim.max.y - lim.min.y)
	props.num = Vector2(int(props.size.x / step), int(props.size.y / step))
	
	
func _create_internal_vert() -> void:
	var limits = area.limits
	var coll = area.coll
	var in_vert_count = 0
	var new_vert = polygon2d.get_polygon()
	
	for y in range(props.num.y + 1): 
		for x in range(props.num.x + 1):
			var point = Vector2(limits.min.x + (x * step), limits.min.y + (y * step))
			if _is_point_in_area(point):
				var is_fit = true
				for vert in polygon2d.get_polygon():
					if point.distance_to(vert) < step / 3:
						is_fit = false
						break
				if is_fit:
					new_vert.append(point)
					in_vert_count += 1
	
	polygon2d.set_polygon(PoolVector2Array(new_vert))
	polygon2d.set_internal_vertex_count(in_vert_count)


func _define_area() -> Dictionary:
	area = {}
	area.area = Area2D.new()
	area.coll = CollisionPolygon2D.new()
	area.area.add_child(area.coll)
	polygon2d.add_child(area.area)
	var poly = polygon2d.get_polygon()
	poly.resize(polygon2d.get_polygon().size() - polygon2d.get_internal_vertex_count())
	area.coll.set_polygon(poly)
	area.limits = _find_area_limits()
	return area


func _triangulate_polygon() -> void:
	var polygon = polygon2d.get_polygon()
	var points = Array(Geometry.triangulate_delaunay_2d(polygon))
	var polygons = []
	
	for i in range(ceil(len(points) / 3)):
		var triangle = []
		for n in range(3):
			triangle.append(points.pop_front())
		var a = polygon[triangle[0]]
		var b = polygon[triangle[1]]
		var c = polygon[triangle[2]]
		
		if _is_triangle_in_area(a, b, c):
			polygons.append(PoolIntArray(triangle))
	
	polygon2d.set_polygons(polygons)
	
	
	
func _is_triangle_in_area(a,b,c):
	var m = Vector2((b.x + c.x) / 2, (b.y + c.y) / 2)
	var mid = Vector2((a.x + m.x) / 2, (a.y + m.y) / 2)
	return true if _is_point_in_area(mid) else false
	
		
		
func _is_point_in_area(point) -> bool:
	var space = area.area.get_world_2d().get_direct_space_state()
	var results = space.intersect_point(point + polygon2d.position, 100, [], area.area.get_collision_layer(), false, true)
	for result in results:
		if result.collider == area.area:
			return true
	return false
	


func _find_area_limits() -> Dictionary:
	var lim = { 
		'min': area.coll.polygon[0],
		'max': area.coll.polygon[0]
	}
	for point in area.coll.polygon:
		if point.x < lim.min.x:
			lim.min.x = point.x
		if point.x > lim.max.x:
			lim.max.x = point.x
		if point.y < lim.min.y:
			lim.min.y = point.y
		if point.y > lim.max.y:
			lim.max.y = point.y
	return lim
	
	
	
func _create_per_vert() -> PoolVector2Array:
	var poly = []
	var init_size = polygon2d.polygon.size()
	for n in range(init_size):
		var next_n = n + 1 if init_size > n + 1 else 0
		var p1 = polygon2d.polygon[n]
		var p2 = polygon2d.polygon[next_n]
		var dir = p1.direction_to(p2)
		var dist = p1.distance_to(p2)
		var num = dist / step
		for _n in range(num + 1):
			var point = p1 + (dir * _n * step)
			if point.distance_to(p2) > step / 3:
				poly.append(point)
					
	polygon2d.set_polygon(PoolVector2Array(poly))
	return polygon2d.get_polygon()


func _is_polygon_selected() -> bool:
	if editor.get_selection().get_selected_nodes():
		var pre_poly = editor.get_selection().get_selected_nodes()[0]
		if pre_poly is Polygon2D:
			return true
		else:
			return false
	else:
		return false


func _selected_nodes():
	return editor.get_selection().get_selected_nodes()
	
	
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# :::::::::::::::::::::::::BONES FUNCTIONS:::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


func _on_weight_bones_pressed():
	var nodes = _selected_nodes()
	for node in nodes:
		if node is Polygon2D and node.skeleton:
			node.clear_bones()
			_sync_bones_to_polygon(node.get_node(node.skeleton), node)
			_weight_bones(node)
			print(node.get_bone_count())
			
	
func _sync_bones_to_polygon(node, poly):
	var sk = poly.get_node(poly.skeleton)
	for ch in node.get_children():
		if ch is Bone2D:
			poly.add_bone(poly.get_path_to(ch), PoolRealArray([]))
		_sync_bones_to_polygon(ch, poly)
			
			
func _weight_bones(poly : Polygon2D) -> void:
	var sk = poly.get_node(poly.skeleton)
	var bones = _define_bones(poly)
	_count_bones_weights(poly, bones)
	
	
func _count_bones_weights(poly, bones):
#	count weights
	for point_n in range(poly.polygon.size()):
		var influent_bones = _get_influent_bones(poly, bones, point_n)
		var all_bones_dist = 0
		for bone_n in range(influent_bones.size()):
			var bone = bones[influent_bones[bone_n].num]
			all_bones_dist += bone.distances[point_n]
		for bone_n in range(influent_bones.size()):
			var bone = bones[influent_bones[bone_n].num]
			if influent_bones.size() == 1:
				bone.weights[point_n] = 1
			else:
				bone.weights[point_n] = 1 - 1 / (all_bones_dist / bone.distances[point_n])

#	set weights
	for bone in bones:
		poly.set_bone_weights(bone.num, PoolRealArray(bone.weights))
	
	
func _get_bone_to_point_distances(poly, bone, ends, centers):
	var distances = []
	for point_n in range(poly.polygon.size()):
		var point = poly.polygon[point_n]
		var dists = []
		for cent_n in range(centers.size()):
			var dist = _get_dist(poly, bone, point, centers[cent_n])
#			var dist = poly.to_local(centers[cent_n]).distance_to(point)
			dists.append(dist)
		distances.append(dists.min())
	return distances
	
	
func _get_dist(par1, par2, p1, p2):
	return par2.to_local(par1.to_global(p1)).distance_to(p2)
	
		
	
func _get_bone_centers(bone, ends):
	var centers = []
	for end in ends:
		centers.append(end / 2)
	return centers
	
	
func _get_bone_ends(bone):
	var ends = []
	var has_bone = false
	for ch in bone.get_children():
		if ch is Bone2D:
			has_bone = true
			ends.append(ch.position)
	if not has_bone:
		ends.append(Vector2(bone.default_length, 0))
	return ends


func _define_bones(poly):
	var sk = poly.get_node(poly.skeleton)
	var bones = []
	bones.resize(poly.get_bone_count())
	for bone_n in range(bones.size()):
		var bone = sk.get_bone(bone_n)
		var ends = _get_bone_ends(bone)
		var centers = _get_bone_centers(bone, ends)
		var distances = _get_bone_to_point_distances(poly, bone, ends, centers)
		var weights = []
		for n in range(distances.size()):
			weights.append(0)
			
		bones[bone_n] = {
			'num': bone_n,
			'bone': bone,
			'ends': ends,
			'centers': centers,
			'distances': distances,
			'weights': weights,
			'bones': null
		}
	for bone_n in range(bones.size()):
		var bone = bones[bone_n]
		bone.bones = _get_bone_to_bone_distances(poly, bone, bones)
	return bones
	
	
func _get_bone_to_bone_distances(poly, bone, bones):
	var dist = []
	dist.resize(bones.size())
	for b in bones:
		var _min
		for bone_c in bone.centers:
			for b_c in b.centers:
				var d = _get_dist(bone.bone, b.bone, bone_c, b_c)
				if not _min or d < _min:
					_min = d
		dist[b.num] = _min
	return dist
	
	
func _get_influent_bones(poly, bones, point_n):
	var arr = []
#	find nearest bone
	var nearest
	for bone in bones:
		if _is_bone_in_poly(bone, poly):
			if nearest:
				if bone.distances[point_n] < nearest.distances[point_n]:
					nearest = bone
			else:
				nearest = bone
	if nearest:
		arr.append({'num': nearest.num, 'dist': nearest.distances[point_n]})
		if nearest.bone.get_parent() is Bone2D:
			if _dist_fit(nearest.bone, nearest.bone.get_parent(), bones, point_n):
				var b = bones[nearest.bone.get_parent().get_index_in_skeleton()]
				arr.append({'num': b.num, 'dist': b.distances[point_n]})
		else:
			for ch in nearest.bone.get_parent().get_children():
				var b = bones[ch.get_index_in_skeleton()]
				if ch is Bone2D and _is_bone_in_poly(b, poly):
					if _dist_fit(nearest.bone, ch, bones, point_n):
						arr.append({'num': b.num, 'dist': b.distances[point_n]})
	#		find nearest bone in parent 
		for ch in nearest.bone.get_children():
			var b = bones[ch.get_index_in_skeleton()]
			if ch is Bone2D and _is_bone_in_poly(b, poly):
				if _dist_fit(nearest.bone, ch, bones, point_n):
					arr.append({'num': b.num, 'dist': b.distances[point_n]})
	return arr
	
	
func _is_bone_in_poly(bone, poly):
	var a = _is_point_in_poly(bone.bone.get_parent(), bone.bone.position, poly)
	var b = false
	var c = false
	for center in bone.centers:
		if _is_point_in_poly(bone.bone, center, poly):
			b = true
			break
	for end in bone.ends:
		if _is_point_in_poly(bone.bone, end, poly):
			c = true
			break
	if a or b or c:
		return true
	else:
		return false


func _is_point_in_poly(point_parent, point, poly):
	var p = poly.to_local(point_parent.to_global(point))
	return true if Geometry.is_point_in_polygon(p, poly.polygon) else false


func _dist_fit(main_b, b, bones, point_n):
	var _m = bones[main_b.get_index_in_skeleton()]
	var _b = bones[b.get_index_in_skeleton()]
	var b_to_point_dist = _b.distances[point_n]
	var m_to_b_dist = _m.bones[_b.num]
	return false if b_to_point_dist > m_to_b_dist else true
	
	
	
	
func _sort_bones_by_dist_to_point(a, b):
	if a.dist < b.dist:
		return true
	else:
		return false


# _________________________________END BONES FUNCTIONS_____________________________-
